"""Compiling with pythran."""

#compiling: pythran sandpile_pythran.py

import numpy as np


#pythran export apply_gravity(int[][])

def apply_gravity(terrain):
    shape = np.shape(terrain)
    while True:
        did_someting = False
        for x, y in np.ndindex(shape):
            if terrain[x, y] >= 4:
                div, terrain[x, y] = divmod(terrain[x][y], 4)
                terrain[x - 1][y] += div
                terrain[x + 1][y] += div
                terrain[x][y + 1] += div
                terrain[x][y - 1] += div
                did_someting = True

        if not did_someting:
            return



#pythran export main(int, bool)

def main(height, show):
    width = int(height ** .5) + 1
    terrain = np.zeros((width, width), dtype=np.int64)
    terrain[width // 2, width // 2] = height
    apply_gravity(terrain)
    if show:
        print(np.array(terrain))
