TARGET=20000

echo "# Profiling Python implementations"
echo
for file in sandpile*.py
do
    printf "## %s\n\n" "$file"
    module="${file%.py}"
    python -c "print(__import__('$module').__doc__)"
    grep '^#compiling:' "$file" | cut -d: -f2- | bash -x
    printf "> "
    python -m pyperf timeit --fast --quiet --setup "from $module import main" "main($TARGET, False)"
    python -c "__import__('$module').main($TARGET, True)" > $module-$TARGET.txt
    echo
    echo
done

sha1sum ./*-$TARGET.txt
rm ./*-$TARGET.txt

echo
echo "# Profiling C implementations"
echo
for file in sandpile.c sandpile_*.c
do
    printf "## %s\n\n" "$file"
    grep '^///' "$file" | cut -b5-
    dest="${file%.c}"
    cc -DNDEBUG -Ofast -W -Wall -pedantic --std=c99 "$file" -lm -lpng -o "$dest"
    rm -f "$dest-$TARGET.png"
    printf "> "
    python -m pyperf command --quiet "./$dest" $TARGET
    ./"$dest" $TARGET "$dest-$TARGET.png"
    echo
    echo
done
sha1sum ./*-$TARGET.png
rm ./*-$TARGET.png
